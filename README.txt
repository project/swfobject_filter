********************************************************************
                     D R U P A L    M O D U L E
********************************************************************
Name: SWFObject Filter Module
Author: Robert Castelo <www.codepositive.com>
Drupal: 6.x
********************************************************************
DESCRIPTION:

SWFObject 2 offers multiple standards-friendly methods to embed SWF files into web pages. It uses JavaScript to detect Flash Player and avoid broken SWF content, and is designed to make embedding SWFs as easy as possible. It supports the use of alternative content to display content to people that browse the web without plug-ins, to help search engines index your content, or to point visitors to the Flash Player download page. SWFObject 2 detection is future-proof, includes Adobe Express Install, and offers an elaborate API for JavaScript developers. All of this is powered by one small (under 10K) JavaScript file.
- http://www.adobe.com/devnet/flashplayer/articles/swfobject.html

SWFObject Filter Module detects Flash being used on a page and automatically loads the SWFObject script into the head of the page. (See USAGE for how to set this up.)

For extra efficiency the SWFObject script is loaded from Google Code, which means one less hit on your server per page load. Additionally if the visitor has already been to a site that linked to the script on Google Code they may have it in their browser cache, thereby reducing the load time of the page.

http://code.google.com/p/swfobject


********************************************************************
INSTALLATION:

Note: It is assumed that you have Drupal up and running.  Be sure to
check the Drupal web site if you need assistance.

1. Place the entire directory into your Drupal directory:
   sites/all/modules/
   

2. Enable the module by navigating to:

   administer > build > modules
     
  Click the 'Save configuration' button at the bottom to commit your
  changes. 

  
********************************************************************
USAGE     

SWFObject Filter module is an input filter which detects a specific string in the body of a node and triggers the inclusion of the SWFObject in the head of the page.

1. Add Input Filter:

    Administer > Site configuration

    Click 'configure' for the input format that is to have this filter applied to it.

    Under 'Filters' enable the SWFObject and save the configuration.

    Repeat for each input format that is to use the filter.

2. Add trigger to page:

    On any field that uses one of the input formats that includes the SWFObject filter add the trigger:

    attributes.id = "swfobject";

    For example:

    <script type="text/javascript">
			var flashvars = {};
			var params = {};
			params.base = "/sites/default/files/flash/";
			var attributes = {};
			attributes.id = "swfobject";
			swfobject.embedSWF("/sites/default/files/flash/xml-gallery.swf", "swfobject", "496", "412", "7.0.0", false, flashvars, params, attributes);
    </script> 




********************************************************************
AUTHOR CONTACT

- Report Bugs/Request Features:
   http://drupal.org/project/swfobject_filter
   
- Comission New Features:
   http://drupal.org/user/3555/contact
   
- Want To Say Thank You:
   http://www.amazon.com/gp/registry/O6JKRQEQ774F

        
********************************************************************
ACKNOWLEDGEMENT

- Initial module development sponsored by Simply Media (www.simply.tv)
